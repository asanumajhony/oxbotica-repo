
const multipleOfThreeMsg = 'Fizz';
const multipleOfFiveMsg = 'Buzz';
const iterations = 100;

for (let i = 0; i < iterations; i++) {
    let message = '';
    if (i%3 === 0) message += multipleOfThreeMsg;
    if (i%5 === 0) message += multipleOfFiveMsg;
    console.log(message || i);
}
