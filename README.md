# Challenge repository


### Task 1
> Write a solution in the most concise way possible in a programming language of your choice. Write a short program that prints each number from 1 to 100 on a new line. For each multiple of 3, print "Fizz" instead of the number. For each multiple of 5, print "Buzz" instead of the number. For numbers which are multiples of both 3 and 5, print "FizzBuzz" instead of the number.

#### Solution:
Implemented the solution at 

    - oxbotica-repo
    └── challenge-1
        └── print-function.js

#### Pre-requisites:

- Installation of Node JS

#### How to execute:

    node challenge-1/print-function.js  

Execution evidence:

![challenge-1](readme/challenge_1.png)


### Task 2
> Derive and create test cases for a simple calculator app. https://testsheepnz.github.io/BasicCalculator.html (select build 2). Carry out your tests and generate a report based on your findings.

#### Solution

The test cases was created on Gherkin format and automated with Cypress and cucumber-preprocessor.

Implemented the solution at

    - oxbotica-repo
    └── cypress

#### Pre-requisites:

- Node JS
- Yarn

#### Installation

I'm using YARN instead of NPM. They have a similar syntax, but both should work.

At project root, execute:

    yarn install

#### Test execution
At project root directory, execute:

    npx cypress open

The first time execution will install Cypress and take may take some time.
When it's ready, a Cypress window will open:

![cypress_open](readme/cypress_open.png)

Click on `basic-calculator.feature`

New browser will open and start test execution.

![basic_calculator](readme/basic_calculator.png)

Note that there are 2 scenarios that is failing with build 2.

You can click on the failure scenario to debug and see each step and why this scenario is failing.

![basic_calculator_failure_scenario](readme/basic_calculator_failure_scenario.png)

Screenshots of the failing scenarios will be saved to:

    - oxbotica-repo
    └── cypress
        └── screenshots
            └── features
                └── basic-calculator.feature

The reporting of this bug at Jira will be something like:

    Title: Basic calculator sum and concatenation calculation error
    
    Description:
    We have found that Basic calculator (https://testsheepnz.github.io/BasicCalculator.html)
    with build 2 is having a calculation error.
    Sum operation:
    < 1st screenshot >
    Concatenate operation:
    < 2nd screenshot >

    Precondition:
    N/A

    Step to reproduce:
    - Navigate to basic calculator url
    - Select Build 2
    - Insert a "4" at First number and "2" at Second number fields
    - Select operation "Sum" and click on "Calculate"
    - Select operation "Concatenate" and click on "Calculate"   

    Expected Behavior:
    Sum: The answer should be "6"
    Concatenate: The answer should be "42"

    Actual Behavior:
    Sum: Showing the answer "42"
    Concatenate: Showing the answer "6"

    Additional information:
    Browser: Chrome
    Version: Version 88.0.4324.182 (Official Build) (64-bit)

    Bug type: Functional
    Environment: Production
    Affected version: current version no.
    Frequency: Always

### Task 3
> Using https://petstore.swagger.io/#/ Create a sample project using Postman for the ‘pet’ part of the sample api.

#### Pre-requisites, Installation and Test Execution will be the same

At Cypress window, click on "pet-shop.feature"

#### Test results:

![test_execution](readme/test_execution.png)

#### Some appointments about this Pet API:
- POST /pet should return 201 with a `Location` header.
- POST /pet should not allow to accept a value at `id` attribute.
- POST /pet without `id` always create the same `id`. E.g: 9222968140497545000. (Running by Cypress, this behaviour not occur on Postman or Curl).
- PUT /pet should be at uri `/pet/{petId}`.
- Workaround for GET /pet/{petId} and DELETE /pet/{petId} was setting a hardcoded `id`.
- POST /pet/{petId} should be a `PUT` method.
- There is not GET for uploaded image.
- There is no integration between uploaded image and `photoUrls`.
- DELETE /pet/{petId} should return a 204.
- POST /pet/{petId}/uploadImage does not validate if `petId` exists.
- After some test execution, upload image by POST `/pet/{petId}/uploadImage` start to return a 400 with message:
`response: "{"code":400,"type":"unknown","message":"org.jvnet.mimepull.MIMEParsingException: java.io.IOException: No space left on device"}"`.
- There is no validation of required fields `name` nad `photoUrls`. (It's even possible to create a PET with an empty payload {}).

#### Postman / Newman

The task 3 was also automated on Postman / Newman at `pet-store-postman` directory.

    - oxbotica-repo
    └── pet-store-postman

#### You can import `pet-store-collection.json` into your Postman and execute a test runner:

![postman_test_run](readme/postman_test_run.png)

> *Note that you need to copy `dog.jpg` file into your Postman working directory due to file upload scenarios.
You con find more about working directory folder [here](https://learning.postman.com/docs/getting-started/settings/#working-directory).

#### Or execute the test run from Newman:

You may need to install newman from npm:

    npm install -g newman

Then at `pet-store-postman` directory, execute:

    newman run pet-store-collection.json -e default.postman_environment.json

![newman_test_run](readme/newman_test_run.png)
