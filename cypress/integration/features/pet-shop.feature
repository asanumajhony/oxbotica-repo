@pet
Feature: Pet api
	Using https://petstore.swagger.io/#/
	Create a sample project using Postman for the ‘pet’ part of the sample api.

	Scenario: Upload a pet image with success
		Given I have a pet with
			| id | 123456789 |
		When I upload a image of this pet with
			| id                 | 123456789   |
			| additionalMetadata | Pet picture |
		Then A response from image upload will return
			| expectedStatus | 200                             |
			| message        | additionalMetadata: Pet picture |

	Scenario: Fail to upload a pet image with an invalid id
		When I try to upload a image of a pet with
			| id                 | invalid     |
			| additionalMetadata | Pet picture |
		Then A response from image upload will return
			| expectedStatus | 404                                                          |
			| message        | java.lang.NumberFormatException: For input string: "invalid" |

	Scenario: Upload a pet image with an non existent id*
		Given I have removed a pet by id
			| id | 123456789 |
		When I try to upload a image of a pet with
			| id                 | 123456789   |
			| additionalMetadata | Pet picture |
		Then A response from image upload will return
			| expectedStatus | 200                             |
			| message        | additionalMetadata: Pet picture |

	Scenario: Create a pet with success
		Given I set tags for a pet
			| tagId | tagName |
			| 10    | male    |
			| 11    | white   |
		When I create a pet with
			| id           | empty     |
			| categoryId   | 1         |
			| categoryName | cat       |
			| name         | Tiger     |
			| status       | available |
		Then The created pet should return
			| statusCode   | 200       |
			| id           | true      |
			| categoryId   | 1         |
			| categoryName | cat       |
			| name         | Tiger     |
			| status       | available |
		And The pet should contain tags
			| tagId | tagName |
			| 10    | male    |
			| 11    | white   |

	Scenario: Fail to create a pet with invalid ID
		When I try to create a pet with
			| id           | invalid   |
			| categoryId   | 1         |
			| categoryName | cat       |
			| name         | Tiger     |
			| status       | available |
		Then A response with fail will return
			| statusCode | 500                    |
			| message    | something bad happened |

	Scenario: Fail to create a pet with invalid category ID
		When I try to create a pet with
			| id           | 123       |
			| categoryId   | invalid   |
			| categoryName | cat       |
			| name         | Tiger     |
			| status       | available |
		Then A response with fail will return
			| statusCode | 500                    |
			| message    | something bad happened |

	Scenario: Fail to create a pet with invalid tag ID
		Given I set tags for a pet
			| tagId   | tagName |
			| invalid | male    |
		When I try to create a pet with
			| id           | 123       |
			| categoryId   | invalid   |
			| categoryName | cat       |
			| name         | Tiger     |
			| status       | available |
		Then A response with fail will return
			| statusCode | 500                    |
			| message    | something bad happened |

	Scenario Outline: Get pets by Status scenarios
		When I get pets by status "<status>"
		Then Pets with status "<status>" should return with success
		Examples:
			| status    |
			| available |
			| pending   |
			| sold      |

	Scenario: Get a pet by id with success
		Given I have a pet with
			| id           | 123456789 |
			| categoryId   | 2         |
			| categoryName | dog       |
			| name         | Max       |
			| status       | sold      |
		When I get a pet by id
		Then The created pet should return
			| statusCode   | 200  |
			| id           | true |
			| categoryId   | 2    |
			| categoryName | dog  |
			| name         | Max  |
			| status       | sold |

	Scenario: Fail to get a pet by invalid Id
		Given I have a pet with
			| id           | 123456789 |
			| categoryId   | 2         |
			| categoryName | dog       |
			| name         | Max       |
			| status       | sold      |
		When I get a pet by id
			| id | invalid |
		Then A response with fail will return
			| statusCode | 404                                                          |
			| message    | java.lang.NumberFormatException: For input string: "invalid" |

	Scenario: Fail to get a pet by non existent Id
		Given I have removed a pet by id
			| id | 123456789 |
		When I get a pet by id
			| id | 123456789 |
		Then A response with fail will return
			| statusCode | 404           |
			| message    | Pet not found |

	Scenario: Update a pet by id with success
		Given I have a pet with
			| id | empty |
		When I update this pet with
			| id           | true    |
			| categoryId   | 3       |
			| categoryName | hamster |
			| name         | brown   |
			| status       | pending |
		Then The updated pet should return
			| statusCode   | 200     |
			| id           | true    |
			| categoryId   | 3       |
			| categoryName | hamster |
			| name         | brown   |
			| status       | pending |

	Scenario: Create a pet by PUT method with success
		Given I have removed a pet by id
			| id | 123456789 |
		When I update this pet with
			| id           | 123456789 |
			| categoryId   | 3         |
			| categoryName | hamster   |
			| name         | brown     |
			| status       | pending   |
		Then The created pet should return
			| statusCode   | 200     |
			| id           | true    |
			| categoryId   | 3       |
			| categoryName | hamster |
			| name         | brown   |
			| status       | pending |

	Scenario: Fail to update a pet by invalid id
		Given I have a pet with
			| id | empty |
		When I update this pet with
			| id           | invalid |
			| categoryId   | 3       |
			| categoryName | hamster |
			| name         | brown   |
			| status       | pending |
		Then A response with fail will return
			| statusCode | 500                    |
			| message    | something bad happened |

	Scenario: Update a pet by form data with success
		Given I have a pet with
			| id | 123456789 |
		When I update this pet with by form data with
			| id     | 123456789 |
			| name   | gold      |
			| status | available |
		Then The pet will be updated with success
			| statusCode | 200       |
			| message    | 123456789 |

	Scenario: Fail to update a pet by form data with invalid id
		When I update this pet with by form data with
			| id     | invalid   |
			| name   | gold      |
			| status | available |
		Then A response with fail will return
			| statusCode | 404                                                          |
			| message    | java.lang.NumberFormatException: For input string: "invalid" |

	Scenario: Fail to update a pet by form data with non existent id
		Given I have removed a pet by id
			| id | 123456789 |
		When I update this pet with by form data with
			| id     | 123456789 |
			| name   | gold      |
			| status | available |
		Then A response with fail will return
			| statusCode | 404       |
			| message    | not found |

	Scenario: Delete a pet by id with success
		Given I have a pet with
			| id | 123456789 |
		When I remove a pet by id
		Then The pet will be deleted with success
			| statusCode | 200       |
			| message    | 123456789 |

	Scenario: Fail to delete a pet with an invalid id
		When I remove a pet by id
			| id | invalid |
		Then A response with fail will return
			| statusCode | 404                                                          |
			| message    | java.lang.NumberFormatException: For input string: "invalid" |

	Scenario: Fail to delete a pet with an non existent id
		Given I have removed a pet by id
			| id | 123456789 |
		When I remove a pet by id
			| id | 123456789 |
		Then A response with fail will return
			| statusCode | 404 |
