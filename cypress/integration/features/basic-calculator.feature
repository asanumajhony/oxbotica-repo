@calculator
Feature: Basic Calculator Validation
	Derive and create test cases for a simple calculator app. https://testsheepnz.github.io/BasicCalculator.html
	(select build 2) Carry out your tests and generate a report based on your findings.

	Scenario Outline: Calculate the Basic Calculator operations with success
		Given I'm at Test Sheep NZ Basic Calculator page and select build "2"
		When I fill the calculator fields with the values and select operation
			| firstNumber  | <firstNumber>  |
			| secondNumber | <secondNumber> |
			| operation    | <operation>    |
		And click on Calculate
		Then I get the answer <answer>
		And Selecting Integer only will get the value of <integersOnly>
		And Answer will be clear after clicking on Clear

		Examples:
			| firstNumber | secondNumber | operation   | answer | integersOnly |
			| 4           | 2            | Add         | 6      | 6            |
			| 4           | 2            | Subtract    | 2      | 2            |
			| 4           | 2            | Multiply    | 8      | 8            |
			| 5           | 2            | Divide      | 2.5    | 2            |
			| 4           | 2            | Concatenate | 42     | false        |
