const resource = '/pet'

function createPet(body) {
  return cy.request({
    method: 'POST',
    failOnStatusCode: false,
    url: `${resource}`,
    headers: {
      'Content-Type': 'application/json',
      accept: 'application/json'
    },
    body: body
  }).as('response');
}

function updatePet(body, petId) {
  return cy.request({
    method: 'PUT',
    failOnStatusCode: false,
    url: `${resource}`,
    headers: {
      'Content-Type': 'application/json',
      accept: 'application/json'
    },
    body: body
  }).as('response');
}

function updatePetByFormData(petId, name, status) {
  return cy.request({
    method: 'POST',
    failOnStatusCode: false,
    url: `${resource}/${petId}`,
    form: true,
    headers: {
      accept: 'application/json'
    },
    body: {
      name,
      status
    }
  }).as('response');
}

function getPetById(petId) {
  return cy.request({
    method: 'GET',
    failOnStatusCode: false,
    url: `${resource}/${petId}`,
    headers: {
      'Content-Type': 'application/json',
      accept: 'application/json'
    }
  }).as('response');
}

function getPetsByStatus(status) {
  return cy.request({
    method: 'GET',
    failOnStatusCode: false,
    url: `${resource}/findByStatus?status=${status}`,
    headers: {
      'Content-Type': 'application/json',
      accept: 'application/json'
    }
  }).as('response');
}

function getRandomDogImage() {
  const url = 'https://dog.ceo/api/breeds/image/random';
  return cy.request(url).then((response) => {
    const { message: imageUrl } = response.body;
    cy.request({
      url: imageUrl,
      encoding: 'base64'
    });
  })
}

function uploadImage(petId, blob, additionalMetadata = '') {
  const formData = new FormData();
  formData.set('file', blob, 'image.jpeg');
  formData.set('additionalMetadata', additionalMetadata);

  const baseUrl = Cypress.env('baseUrl');
  const url = `${baseUrl}${resource}/${petId}/uploadImage`;
  cy.intercept(url).as('xhrResponse');
  cy.form_request('POST', url, formData, ({ response }) => {
    expect(response).to.be.not.null;
  });
}

function deletePetById(petId) {
  return cy.request({
    method: 'DELETE',
    failOnStatusCode: false,
    url: `${resource}/${petId}`,
    headers: {
      'Content-Type': 'application/json',
      accept: 'application/json'
    }
  }).as('response');
}

export default {
  createPet,
  updatePet,
  updatePetByFormData,
  deletePetById,
  getPetById,
  getPetsByStatus,
  getRandomDogImage,
  uploadImage
};
