class PetBean {
  constructor(
    id,
    category = {
      id: 10,
      name: 'dog'
    },
    name = 'Max',
    photoUrls = [],
    tags = [],
    status = 'available'
  ) {
    this.id = id;
    this.category = category;
    this.name = name;
    this.photoUrls = photoUrls;
    this.tags = tags;
    this.status = status;
  }

  setId(id) {
    if (id) this.id = id;
    return this;
  }

  setCategory(categoryId, categoryName) {
    if (categoryId) this.category = {
      id: categoryId,
      name: categoryName
    };
    return this;
  }

  setName(name) {
    if (name) this.name = name;
    return this;
  }

  pushPhotoUrls(photoUrl) {
    if (photoUrl) this.photoUrls.push(photoUrl)
    return this;
  }

  setTags(tags) {
    if (tags) this.tags = tags;
    return this;
  }

  pushTags(tagId, tagName) {
    if (tagId) this.tags.push({
      id: tagId,
      name: tagName
    })
    return this;
  }

  setStatus(status) {
    if (status) this.status = status;
    return this;
  }
}

module.exports = PetBean;
