import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';

const {
  createPet, getPetById, getRandomDogImage, uploadImage,
  getPetsByStatus, updatePet, deletePetById, updatePetByFormData
} = require('../page_objects/pet-api-page');

const PetBean = require('../dto/PetBean');

Given('I set tags for a pet', function (table) {
  const data = table.hashes();
  const tags = [];
  data.forEach((tag) => {
    const { tagId, tagName } = tag;
    tags.push({
      id: tagId,
      name: tagName
    })
  })
  cy.wrap(tags, { log: false }).as('tags');
});

When('I( try to) create/have a pet with', function (table) {
  const {
    id, categoryId, categoryName, name, status
  } = table.rowsHash();
  let petId = '';
  if (id !== 'empty') petId = id;

  const pet = new PetBean()
    .setId(petId)
    .setCategory(categoryId, categoryName)
    .setName(name)
    .setTags(this.tags)
    .setStatus(status);

  createPet(pet).then((response) => {
    if (response.status === 200) {
      cy.wrap(response.body.id, { log: false }).as('petId');
    }
  });
});

Then('The created/updated pet should return', function (table) {
  const {
    statusCode, id, categoryId, categoryName, name, status
  } = table.rowsHash();

  cy.get('@response').then((response) => {
    const { body } = response;
    expect(response.status).to.be.eq(parseInt(statusCode, 10));
    if (id === 'true') {
      expect(body.id).to.be.eq(this.petId);
    } else {
      expect(body.id).to.be.eq(id);
    }
    if (name) expect(body.name).to.be.eq(name);
    if (status) expect(body.status).to.be.eq(status);
    if (categoryId) expect(body.category.id).to.be.eq(parseInt(categoryId, 10));
    if (categoryName) expect(body.category.name).to.be.eq(categoryName);
  })
});

Then('The pet should contain tags', function (table) {
  const data = table.hashes();
  cy.get('@response').then((response) => {
    data.forEach((tag) => {
      const { tagName } = tag;
      const tagId = parseInt(tag.tagId, 10);
      const { tags } = response.body
      const found = tags.find((expectedTag) => expectedTag.id === tagId, 10);
      if (!found) {
        throw new Error(`Expected tag not found: tagId: ${tagId}`);
      }
      expect(found.id, 'tag.id').to.be.eq(tagId);
      expect(found.name, 'tag.name').to.be.eq(tagName);
    })
  })
});

When('I (try to )upload a image of this/a pet with',  function (table) {
  const { additionalMetadata, id, expectedStatus } = table.rowsHash();
  getRandomDogImage().then((response) => {
    const rawImage = response.body;
    const fileType = response.headers['content-type'];
    const blob = Cypress.Blob.base64StringToBlob(rawImage, fileType);
    uploadImage(id, blob, additionalMetadata)
  })
});

Then('I get a pet by id', function (table) {
  let id;
  if (table) {
    id = table.rowsHash().id;
  }
  getPetById(id || this.petId);
});

When('I get pets by status {string}', function (status) {
  getPetsByStatus(status);
});

Then('Pets with status {string} should return with success', function (status) {
  cy.get('@response').then((response) => {
    cy.log('Pets amount:', response.body.length);
    response.body.forEach((pet, index) => {
      expect(pet.status, `Pet ${index + 1} Status`).to.be.eq(status);
    })
  })
});

When('I update this pet with', function (table) {
  const {
    id, categoryId, categoryName, name, status
  } = table.rowsHash();
  let petId;
  if (id === 'true') {
    petId = this.petId;
  }

  const pet = new PetBean()
    .setId(petId || id)
    .setCategory(categoryId, categoryName)
    .setName(name)
    .setTags(this.tags)
    .setStatus(status);

  updatePet(pet).then((response) => {
    if (response.status === 200) {
      cy.wrap(response.body.id, { log: false }).as('petId');
    }
  });
});

When('I update this pet with by form data with', function (table) {
  const {
    id, name, status
  } = table.rowsHash();
  let petId = id;
  if (id === 'true') {
    petId = this.petId;
  }

  updatePetByFormData(petId, name, status)
});

When('I (have )remove(d) a pet by id', function (table) {
  let id;
  if (table) {
    id = table.rowsHash().id;
  }
  deletePetById(id || this.petId);
});

Then('The pet will be updated/deleted with success', function (table) {
  const { statusCode, message } = table.rowsHash();
  cy.get('@response').then((response) => {
    expect(response.status).to.be.eq(parseInt(statusCode, 10));
    expect(response.body.message).to.be.eq(message);
  })
});

Then('A response with fail will return', function (table) {
  const { statusCode, message } = table.rowsHash();
  cy.get('@response').then((response) => {
    expect(response.status).to.be.eq(parseInt(statusCode, 10));
    if (message) expect(response.body.message).to.be.eq(message);
  })
});

Then(/^A response from image upload will return$/, function (table) {
  const { expectedStatus, message } = table.rowsHash();
  cy.wait('@xhrResponse').then(({ response }) => {
    expect(response.statusCode, 'upload image').to.eq(parseInt(expectedStatus)  );
    const { body } = response;
    if (expectedStatus === '200') {
      expect(body.message).to.includes(message);
    } else {
      expect(body.message).to.be.eq(message);
    }
  });
});
