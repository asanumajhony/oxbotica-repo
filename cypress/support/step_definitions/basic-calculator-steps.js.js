import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';

Given("I'm at Test Sheep NZ Basic Calculator page and select build {string}", function (buildNumber) {
  cy.visit('https://testsheepnz.github.io/BasicCalculator.html');
  cy.get('#selectBuild').select(buildNumber);
});

When('I fill the calculator fields with the values and select operation', function (table) {
  const { firstNumber, secondNumber, operation } = table.rowsHash();

  cy.get('#number1Field').type(firstNumber);
  cy.get('#number2Field').type(secondNumber);
  cy.get('#selectOperationDropdown').select(operation);
});

When('click on Calculate', function () {
  cy.get('#calculateButton').click();
  cy.get('#loadEquipment').should('not.be.visible');
});

Then('I get the answer {int}', function (answer) {
  cy.get('#numberAnswerField').should('have.value', answer);
});

Then('I get the answer {float}', function (answer) {
  cy.get('#numberAnswerField').should('have.value', answer);
});

Then('Selecting Integer only will get the value of {word}', function (integerOnlyResult) {
  if (integerOnlyResult !== 'false') {
    cy.get('#integerSelect').check();
    cy.get('#numberAnswerField').should('have.value', integerOnlyResult);
  }
});

Then('Answer will be clear after clicking on Clear', function () {
  cy.get('#clearButton').click();
  cy.get('#numberAnswerField').should('be.empty');
});
